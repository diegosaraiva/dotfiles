# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.
 
# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

#-------------------------#
# BASE - UTILITY          #
#-------------------------#
exists() {
    test -x "$(command -v "$1")"
}

# Set architecture flags
export ARCHFLAGS="-arch x86_64"

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export PATH=/usr/local/bin:$PATH

#Python stuffs
export WORKON_HOME=$HOME/Virtualenvs
# pip ---------------------------------------------------------
# pip should only run if there is a virtualenv currently activated
export PIP_REQUIRE_VIRTUALENV=true
# cache pip-installed packages to avoid re-downloading
export PIP_DOWNLOAD_CACHE=$HOME/.pip/cache
export PIP_VIRTUALENV_BASE=$WORKON_HOME
export PIP_RESPECT_VIRTUALENV=true
# virtualenvwrapper -------------------------------------------
export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python2.7
export VIRTUALENVWRAPPER_VIRTUALENV_ARGS='--no-site-packages'
if [[ -r /usr/local/bin/virtualenvwrapper.sh ]]; then
    source /usr/local/bin/virtualenvwrapper.sh
else
    echo "WARNING: Can't find virtualenvwrapper.sh"
fi
# If in the future we want to upgrade our global packages,
# the above function enables us to do so via:
# syspip install --upgrade pip setuptools virtualenv
syspip(){
    PIP_REQUIRE_VIRTUALENV="" pip "$@"
}
# postgres settings  -------------------------------------------
export PGDATA=/usr/local/var/postgres
