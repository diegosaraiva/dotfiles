# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="agnoster"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Uncomment this to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
# DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(cp gitfast git-extras osx brew python pylint pip postgres virtualenv virtualenvwrapper jump history-substring-search zsh-syntax-highlighting)

# Customize to your needs...
source ~/.profile #Get path
source ~/.shell/aliases
source ~/.shell/functions
source ~/.shell/variables

function battery {
    if [ -e ~/bin/batterycharge.sh ]
    then
        echo `sh ~/bin/batterycharge.sh`
    else
        echo ''
    fi
}
#RPROMPT='%{$fg[red]%}【$(battery)% 】%{$fg[white]%}'
source $ZSH/oh-my-zsh.sh
autoload -U compinit && compinit

#Alias for shell config refresh:
alias refresh='source ~/.zshrc'

#
# My own custom aliases bellow:
#
alias smartmake='make clean; clear; make;'
# alias templight='clang++-templight -templight -templight-safe-mode -templight-format xml'
# alias templight11='templight -std=c++11'
#alias g++11='g++ -std=c++11'
alias clang++14='clang++ -std=c++1y'
alias c++='clang++'
alias c++14='clang++14'
#
