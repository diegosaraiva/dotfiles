require 'formula'

# Documentation: https://github.com/mxcl/homebrew/wiki/Formula-Cookbook
# PLEASE REMOVE ALL GENERATED COMMENTS BEFORE SUBMITTING YOUR PULL REQUEST!

class Wt < Formula
    homepage 'http://www.webtoolkit.eu/'
    url 'http://downloads.sourceforge.net/witty/wt-3.3.0.tar.gz'
    sha1 'da3ad190e51d4e2bd2851141b27dd70b272cbb2c'

    depends_on 'cmake' => :build
    depends_on 'boost'
    depends_on 'libharu'


    #optionals 
    depends_on 'openssl' => :optional
    depends_on 'zlib' => :optional
    depends_on 'fcgi' => :optional
    depends_on 'postgresql'=> :optional
    depends_on 'sqlite'=> :optional
    depends_on 'graphicsmagick'=> :optional
    depends_on 'pango'=> :optional
    depends_on 'mysql++'=> :optional
    depends_on 'qt4'=> :optional

    option :unviversal
    option 'with-ssl','for SSL support in built-in webserver'
    option 'with-zip','for SSL support in built-in webserver'

    def install
        # ENV.j1  # if your formula's build system can't parallelize

        #system "./configure", "--disable-debug", "--disable-dependency-tracking",
        #                      "--prefix=#{prefix}"
        #CONFIGDIR: Path for configuration files (default is /etc/wt/)
        std_cmake_args = %W[    
            .
            -DCMAKE_INSTALL_PREFIX=#{prefix}
            -DCONNECTOR_HTTP=ON
            -DWEBUSER=http
            -DWEBGROUP=http
            -DUSE_SYSTEM_SQLITE3=ON
            -DINSTALL_EXAMPLES=ON
            -DCONFIGDIR=#{etc}/wf
        ]
        system "cmake", ".", *std_cmake_args
        system "make", "install" # if this fails, try separate make/make install steps
    end

    test do
        # `test do` will create, run in and delete a temporary directory.
        #
        # This test will fail and we won't accept that! It's enough to just replace
        # "false" with the main program this formula installs, but it'd be nice if you
        # were more thorough. Run the test with `brew test wt`.
        system "false"
    end
end
