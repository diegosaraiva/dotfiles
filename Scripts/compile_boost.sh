#!/bin/bash
# The default naming was changed a while ago. There are 3 options.
#--layout=system does not add any suffixes. It is the default except on Windows.
#--layout=tagged includes the variant suffixes.
#--layout=versioned includes the variant and the Boost version. It is the default on Windows.
brew install boost --with-c++11 --with-icu 
